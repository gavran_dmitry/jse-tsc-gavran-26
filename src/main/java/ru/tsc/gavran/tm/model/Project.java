package ru.tsc.gavran.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.entity.IWBS;
import ru.tsc.gavran.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractOwnerEntity implements IWBS {

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    private Date created = new Date();

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}